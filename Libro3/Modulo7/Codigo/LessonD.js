
var grid = [    
[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
[0,'1',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
[0,'1',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
[0,'1',0,0,0,0,0,'3',0,0,0,0,0,0,0,0,0,0,0],
[0,'1,2','2','2','2','2','2','2,3','2',0,0,0,0,0,0,0,0,0,0],
[0,'1',0,0,0,0,0,'3',0,0,0,0,0,0,0,0,0,0,0],
[0,'1',0,0,0,0,0,'3',0,0,0,0,0,'4',0,0,0,0,0],
[0,'1',0,0,0,0,0,'3',0,'10',0,0,0,'4',0,0,0,0,0],
[0,'1',0,0,'7','7','7','3,7','7','7,10',0,0,0,'4',0,0,0,0,0],
[0,0,0,0,0,0,0,'3',0,'10',0,'8,9','9','4,9','9','9','9','9',0],
[0,0,0,0,0,0,0,'3',0,'10',0,'8',0,0,0,0,0,0,0],
[0,0,0,0,0,0,0,'3',0,'6,10','6','6,8','6','6','6','6','6',0,0],
[0,0,0,0,0,0,0,0,0,'10',0,'8',0,0,0,0,0,0,0],
[0,0,0,0,0,0,0,0,0,'10',0,'8',0,0,0,0,0,0,0],
[0,0,0,0,0,'5','5','5','5','5,10','5',0,0,0,0,0,0,0,0],
[0,0,0,0,0,0,0,0,0,'10',0,0,0,0,0,0,0,0,0],
[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
           ];

var preguntas = [
"There's a new type of flu that could cause an e____________.",
"Will you make a d________ for the new school hall?.",
"Steve is a v_________ firefighter.",
"We must ______ for the old and weak.",
"There are lots of hidden d________ in our homes.",
"Mark was e________ privately at home.",
"They managed to escape the _______ attacks and return home safely.",
"The goverment has to take _______ measures against unemployment.",
"The school nurse ________ the scratch on my arm.",
"Researchers are trying to find the cure for many i_________.",
            ];
var respuestas = [
"epidemic",//1
"donation",//2
"volunteer",//3
"care",//4
"danger",//5
"educated",//6
"brutal",//7
"tough",//8
"treated",//9
"illnesses"//10
];



var noChecks=0;
var totalPistas=4;
var pistas=0;
var verticales=new Array();
var cordenadasHorizontales= new Array();
var cordenadasVerticales= new Array();
var horizontales= new Array();
var foco;


crearCrucigrama();
$("td").css({
   "width": "30px",
   "height": "30px",
   "font-size": "25px"
});

$('#Verticales').css('font-size','20px');
$('#Horizontales').css('font-size','20px');


function crearCrucigrama (tamano) {
    for (var i = 0; i < grid.length; i++) {
        $('#tablaCrucigrama').append("<tr id='fila"+i+"'");
        for (var j = 0; j < grid[i].length; j++) {
            if (grid[i][j]==0) {
                $('#fila'+i).append("<td  id='"+i+"-"+j+"'> </td>");
            }else {//hay algun numero 
                $('#fila'+i).append("<td class='borde td' id='"+i+"-"+j+"'>  <div class='casilla' contenteditable='true'></div></td>");
                var numero= grid[i][j].toString().split(",");
                var izquierda= grid[i][(j-1)].toString().split(",");
                if (j<grid[i].length-1) {
                    var derecha= grid[i][j+1].toString().split(",");    
                }   
                var arriba= grid[i-1][j].toString().split(",");
                if (i<grid.length-1) {
                    var abajo= grid[i+1][j].toString().split(",");
                }
                //Añadir Verticales
                var cordenadaArriba=(i-1)+"-"+j;
                var cordenadaActual=i+"-"+j;
                if (arriba==0 && numero[0]==abajo[0] && abajo!=0){
                    //$('#td'+(i-1)+"-"+j).append(numero[0] +"&#8595;");
                    //$('#td'+(i-1)+"-"+j).html("<span class='td-md borde' id='td"+(i-1)+"-"+j+"'>"+numero[0]+" &#8595</span>"); 
                    $('#'+cordenadaArriba).append("<span class='noPregunta sp'>"+numero[0]+" &#8595 </span>");
                    $('#Verticales').append(numero[0]+".-"+preguntas[numero[0]-1] +"<br>");
                    verticales.push(numero[0]);
                    cordenadasVerticales.push(cordenadaActual);
                }else if (arriba==0 && numero[1]== abajo[0] && abajo!=0 ) {
                    //$('#td'+(i-1)+"-"+j).html("<td class='td-md borde' id='td"+(i-1)+"-"+j+"'>"+numero[1]+" &#8595</td>");
                    //$('#td'+(i-1)+"-"+j).append(numero[1] +'&#8595;');
                    $('#'+cordenadaArriba).append("<span class='noPregunta sp' >"+numero[1]+"&#8595</span>");
                    $('#Verticales').append(numero[1]+".-"+preguntas[numero[1]-1] +"<br>");
                    verticales.push(numero[1]);
                    cordenadasVerticales.push(cordenadaActual);
                }else if (arriba==0 && numero[0] == abajo[1] && abajo!=0 ) {
                    //$('#td'+(i-1)+"-"+j).append(numero[0] +"&#8595;");
                    //$('#td'+(i-1)+"-"+j).html("<span class='td-md borde' id='td"+(i-1)+"-"+j+"'>"+numero[0]+" &#8595</span>");
                    $('#'+cordenadaArriba).append("<span class='noPregunta sp'>"+numero[0]+" &#8595</span>");
                    $('#Verticales').append(numero[0]+".-"+preguntas[numero[0]-1] +"<br>");
                    verticales.push(numero[0]);
                    cordenadasVerticales.push(cordenadaActual);
                }
                //Añadir Horizontales
                var cordenadaIzquierda=i+"-"+(j-1);
                if(izquierda==0 && numero[0]==derecha[0] && derecha!=0 ){
                    //$('#td'+(i)+""+(j-1)).html("<td class='td-md-plus borde' id='td"+i+""+(j-1)+"'>"+grid[i][j]+" &#8594</td>");
                    //$('#td'+i+"-"+(j-1)).html("<td class='td-md borde'>"+numero[0]+"&#8594 </td>");
                    //('#td'+i+"-"+(j-1)).append(numero[0] +"&#8594;");
                    $('#'+cordenadaIzquierda).append("<span class='noPregunta sp'>"+ numero[0] +" &#8594</span>");
                    $('#Horizontales').append(numero[0]+".-"+preguntas[numero[0]-1] +"<br>");
                    horizontales.push(numero[0]);
                    cordenadasHorizontales.push(cordenadaActual);
                }else if (izquierda==0 && numero[1]==derecha[0] && derecha!=0) {
                    //$('#td'+i+"-"+(j-1)).html("<td class='td-md'>"+numero[1]+"&#8594 </td>");
                    //('#td'+i+"-"+(j-1)).append(numero[1]+" &#8594;");
                    $('#'+cordenadaIzquierda).append("<span class='noPregunta sp'>"+ numero[1] +" &#8594</span>");
                    $('#Horizontales').append(numero[1]+".-"+preguntas[numero[1]-1] +"<br>");
                    horizontales.push(numero[1]);
                    cordenadasHorizontales.push(cordenadaActual);
                }else if (izquierda==0 && numero[0]==derecha[1] && derecha!=0) {
                    //$('#td'+i+"-"+(j-1)).html("<td class='td-md'>"+numero[1]+"&#8594 </td>");
                    //('#td'+i+"-"+(j-1)).append(numero[1]+" &#8594;");
                    $('#'+cordenadaIzquierda).append("<span class='noPregunta sp'>"+ numero[0] +" &#8594</span>");
                    $('#Horizontales').append(numero[0]+".-"+preguntas[numero[0]-1] +"<br>");
                    horizontales.push(numero[0]);
                    cordenadasHorizontales.push(cordenadaActual);
                }else if (izquierda==0 && numero[1]==derecha[1] && derecha!=0 && numero[1]!=null) {
                    //$('#td'+i+"-"+(j-1)).html("<td class='td-md'>"+numero[1]+"&#8594 </td>");
                    //('#td'+i+"-"+(j-1)).append(numero[1]+" &#8594;");
                    $('#'+cordenadaIzquierda).append("<span class='noPregunta sp'>"+ numero[1] +" &#8594</span>");
                    $('#Horizontales').append(numero[1]+".-"+preguntas[numero[0]-1] +"<br>");
                    horizontales.push(numero[1]);
                    cordenadasHorizontales.push(cordenadaActual);
                }
                            
            }
        }
        $('#tablaCrucigrama').append("</td>");
    }
    $('#tablaCrucigrama').append("<tr> </tr>");
}



//variables globales
var cordenada;
var noPregunta;
var direccionRespuesta;

/*$('.noPregunta').click(function(){
    $('.noPregunta').css('background','#e3e7ed')
    $(this).css('background', '#A4A4A4');
    cordenada=this.parentElement.id.split("-");
    noPregunta=$(this)[0].innerText.substring(0, 1);
    $("#Verificar").removeAttr('disabled');
    direccionRespuesta= getPosition(noPregunta);
    if (direccionRespuesta=="vertical") {
        var nuevaCordedana=(parseInt(cordenada[0])+1)+"-"+cordenada[1];
        $('#'+nuevaCordedana).children().focus();
        foco= nuevaCordedana.split("-");
    }else if (direccionRespuesta=="horizontal") {
        var nuevaCordedana=cordenada[0]+"-"+(parseInt(cordenada[1])+1);
        $('#'+nuevaCordedana).children().focus();
        foco= nuevaCordedana.split("-");
    }
});*/


$('#Verificar').click(function () {
    for (var i = 0; i < horizontales.length; i++) {
        var resp= respuestas[horizontales[i]-1];
        var id= cordenadasHorizontales[i].split("-");
        for (var j = 0; j < resp.length; j++) {
            var letra= $('#'+id[0]+'-'+(parseInt(id[1])+j))[0].innerText;   
            if (letra==resp[j]) {
                $('#'+id[0]+'-'+(parseInt(id[1])+j)).children().css('background',"#01DF01");
            }else if(letra!= resp[j] && letra!="") {
                $('#'+id[0]+'-'+(parseInt(id[1])+j)).children().css('background',"#FF0000");
            }else if(letra==""){
                $('#'+id[0]+'-'+(parseInt(id[1])+j)).children().css('background',"#FFF");
            }
        }
    }
    for (var i = 0; i < verticales.length; i++) {
        var resp= respuestas[verticales[i]-1];
        var id= cordenadasVerticales[i].split("-");
        for (var j = 0; j < resp.length; j++) {
            var letra= $('#'+(parseInt(id[0])+j)+"-"+id[1])[0].innerText;   
            if (letra==resp[j]) {
                $('#'+(parseInt(id[0])+j)+"-"+id[1]).children().css('background',"#01DF01");
            }else if(letra!= resp[j] && letra!="") {
                $('#'+(parseInt(id[0])+j)+"-"+id[1]).children().css('background',"#FF0000");
            }else if(letra==""){
                $('#'+(parseInt(id[0])+j)+"-"+id[1]).children().css('background',"#FFF");
            }
        }
    }
    noChecks++;
    $('#noChecks').html(' Numero de Checks='+ noChecks);
});
$('.casilla').focus(function () {
    foco= this.parentElement.id.split("-");
});
$('.casilla').click(function () {
    var cordenada=this.parentElement.id.split("-");
    var numeroRespuesta= grid[cordenada[0]][cordenada[1]];
    foco= cordenada;
    direccionRespuesta=getPosition(numeroRespuesta);
    if (direccionRespuesta=="vertical") {
        var nuevaCordedana=(parseInt(cordenada[0])+1)+"-"+cordenada[1];
        //$('#'+nuevaCordedana).children().focus();
        foco= nuevaCordedana.split("-");
    }else if (direccionRespuesta=="horizontal") {
        var nuevaCordedana=cordenada[0]+"-"+(parseInt(cordenada[1])+1);
        //$('#'+nuevaCordedana).children().focus();
        foco= nuevaCordedana.split("-");
    }
});

//@param1 (int pregunta)
function getPosition(pregunta) {
    for (var i = 0; i < verticales.length; i++) {
        if (pregunta== verticales[i]) {
            return "vertical";
        }
    }
    for (var i = 0; i < horizontales.length; i++) {
        if (pregunta==horizontales[i]) {
            return "horizontal";
        }
    }
}


var tecla;
$(".casilla").keyup(function(){
    var id=this.parentElement.id.split("-");
    if (tecla!=40 && tecla!=39 && tecla!=38 && tecla!=37 && tecla!=8 && tecla!=17 && tecla!=18 && tecla!=16 && tecla!=9 && tecla!=33 && tecla!=34 && tecla!=13 && tecla!=46 ) {
        if(tecla==32){
            var identificador=this.parentElement.id;
            $('#'+identificador ).children().text('_');
        }
        if (direccionRespuesta=="vertical" ) {
            var nuevoId= parseInt(id[0])+1 +"-"+id[1];
            $('#'+nuevoId).children().focus();
        }else if (direccionRespuesta=="horizontal") {
            var nuevoId=id[0]+"-"+(parseInt(id[1])+1);
            $('#'+nuevoId).children().focus();
        }
        this.id="casillaOcupada";
    }else if(tecla==13 || tecla==46 || tecla==8){
        var id=this.parentElement.id;
        $('#'+id).children().text("");

    }
});
$('.casilla').keydown(function(e){
    var id=this.parentElement.id.split("-");
    tecla= e.which;
    if(tecla==40){
        //tecla abajo
        var nuevoId= parseInt(id[0])+1 +"-"+id[1];
        $('#'+nuevoId).children().focus();
        direccionRespuesta="vertical";
    }else if(tecla==38){
        //tecla arriba
        var nuevoId= parseInt(id[0])-1 +"-"+id[1];
        $('#'+nuevoId).children().focus();
        direccionRespuesta="vertical";
    }else if(tecla==37){
        //tecla izquierda
        var nuevoId= id[0] +"-"+ (parseInt(id[1])-1);
        $('#'+nuevoId).children().focus();
        direccionRespuesta="horizontal";
    }else if(tecla==39){
        //tecla derecha
        var nuevoId= id[0]+"-"+(parseInt(id[1])+1);
        $('#'+nuevoId).children().focus();
        direccionRespuesta="horizontal";
    }else if(tecla==8 || tecla==46 || tecla==13){
        this.id="casillaVacia"
    }
});
$('.casilla').keypress(function(){
    if (tecla!=13) {
        $(this).text("");
    }   
});