<html>
   <head>
        <title>Modulo 4: LessonE</title>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
        <link rel="stylesheet" type="text/css" href="..\..\bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="..\..\estilos.css">
        <link rel="stylesheet" type="text/css" href="..\..\Estilos/estilosIndex.css">
    </head>
<body bgcolor="#e3e7ed">
    <!--banner-->
     <!--Small screens header-->
     <div class="container-fluid hidden-sm-up" >
       <div class="row" align="center" >
          <div class="col-sm-12 col-xs-12 titulo "  align="left"><img align="left" src="..\..\Imagenes/manitas.png" height="40" width="70">
           <div class="titulo h4"><span class="colorLetra" style="margin-left: 20px">B</span>ritish <span class="colorLetra">C</span>ertification <span class="colorLetra">C</span>lub</div>
           </div>
     </div>
     </div>
        <!--+Medium screens header-->
   <div class="container-fluid hidden-xs-down " >
         <div class="row" >
             <div class="col-sm-12 col-md-7 col-lg-7  titulo h2" ><img align="left" src="..\..\Imagenes/manitas.png" height="45" width="80"><span class=" colorLetra" style=" margin-left: 15px;">B</span>ritish <span class="colorLetra">C</span>ertification <span class="colorLetra">C</span>lub</div>
             <div class="col-sm-5 col-md-5 col-lg-5 hidden-sm-down  " align="right"><img src="..\..\Imagenes/cambrige_logo.png" width="130px" height="80px"></div>
         </div>
    </div>
    <hr>
     <!--Bannner-->
    <div class="container-fluid col-xs-12 col-lg-12  hidden-sm-up" style="text-align: center;">
        <h3> Modulo 4: Lesson E</h3>    
    </div>
    <div class="container-fluid col-lg-12  hidden-xs-down" style="text-align: center;">
        <h1> Modulo 4: Lesson E</h1>    
    </div>
    <hr>
    
    <body style="background-color: #e3e7ed">
        <div class="container-fluid" >
            <div class="row">
                <div class="col-md-6 "  align="center" id="divCrucigrama">
                    <table class="puzzle borde" id="tablaCrucigrama"  align="center">
                
                    </table>
                </div>
                <div class="col-md-6 " id='preguntas'>
                    <div id="Verticales" >
                        <h2>Verticales:</h2>
                    
                    </div>
                    <hr>
                    <div  id="Horizontales">
                        <h2>Horizontales:</h2>
                    </div>
                    <hr>
                    <div class="Botones">
                        <input type="button" id="Limpiar" value="Clear all">
                        <input type="button" id="Verificar" value="Check" >
                        <input type="button" id="Pista" value="Clue">
                    </div>
                    <hr>
                    <div id="noChecks">
                        
                    </div>
                </div>
            </div>
        </div>
        <script src="Codigo/LessonE.js"></script>
    </body>
</html>