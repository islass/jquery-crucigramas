

var grid = [	
        [0,0,0,0,0,0,0,0,0,0,0],
		[0,0,0,'2',0,0,0,'7',0,'1',0],
		[0,0,0,'2','4',0,0,'7',0,'1',0],
		[0,0,0,'2','4',0,0,'7',0,'1',0],
		[0,0,0,'2','4',0,'5','5,7','5','5,1',0],
		[0,'10','10','2,10','4,10',0,0,'7',0,0,0],
		[0,0,0,'2',0,0,0,'7',0,0,0],
		[0,0,0,0,0,0,'3,11','11','7,11','11',0],
		[0,0,0,0,0,'8','3','7',0,0,0],
		[0,0,0,0,0,'8','3',0,0,0,0],
		[0,0,0,'14',0,'8','3',0,0,0,0],
		[0,0,'9','9,14','9','8,9','3,9',0,0,'13',0],
		[0,0,0,'14',0,0,0,'6,15','15','13,15','15'],
		[0,0,0,'14',0,0,0,'6',0,'13',0],
		[0,0,0,0,0,'12','12','6,12','12','12,13',0],
		[0,0,0,0,0,0,0,'6',0,0,0],
		[0,0,0,0,0,0,0,0,0,0,0]
];


var preguntas = ["Susan has got two ______: a dog and a cat", 
			"There's a ______ in  my room.",
			"¿Lance's _____ are very white. ",
			"Dorie has long ____.",
			"A cat has got four _____.",
			"Give me your _____ and let's go.",
			"Our pet dog has a ________.",
			"My brother has got very big _____. ",
			"Open your______!.",
			"Samantha's got green ______.",
			"The mouse has got a long _____.",
			"Birds have got ______ .",
			"A ______ has big ______. ",
			"A snake has got a long ______. ",
            "I fell and hit my _____ on the table."
			
            ];
var respuestas = ["Pets", //1
             "Spider",//2
             "Teeth",//3
             "Arms",//4
             "Legs",//5
             "Hand",//6
             "Name_tag",//7
			 "Feet",//8
			 "Mouth",//9
			 "Eyes",//10
			 "Tail",//11
			 "Wings",//12
			 "Ears",//13
			 "Body",//14
			 "Head"//15
            ];

var noChecks=0;
var totalPistas=4;
var pistas=0;
var verticales=new Array();
var cordenadasHorizontales= new Array();
var cordenadasVerticales= new Array();
var horizontales= new Array();
var foco;


crearCrucigrama();
$("td").css({
   "width": "40px",
   "height": "40px",
   "font-size": "25px"
});

$('#Verticales').css('font-size','25px');
$('#Horizontales').css('font-size','25px');


function crearCrucigrama (tamano) {
    for (var i = 0; i < grid.length; i++) {
        $('#tablaCrucigrama').append("<tr id='fila"+i+"'");
        for (var j = 0; j < grid[i].length; j++) {
            if (grid[i][j]==0) {
                $('#fila'+i).append("<td  id='"+i+"-"+j+"'> </td>");
            }else {//hay algun numero 
                $('#fila'+i).append("<td class='borde td' id='"+i+"-"+j+"'>  <div class='casilla' contenteditable='true'></div></td>");
                var numero= grid[i][j].toString().split(",");
                var izquierda= grid[i][(j-1)].toString().split(",");
                if (j<grid[i].length-1) {
                    var derecha= grid[i][j+1].toString().split(",");    
                }   
                var arriba= grid[i-1][j].toString().split(",");
                if (i<grid.length-1) {
                    var abajo= grid[i+1][j].toString().split(",");
                }
                //Añadir Verticales
                var cordenadaArriba=(i-1)+"-"+j;
                var cordenadaActual=i+"-"+j;
                if (arriba==0 && numero[0]==abajo[0] && abajo!=0){
                    //$('#td'+(i-1)+"-"+j).append(numero[0] +"&#8595;");
                    //$('#td'+(i-1)+"-"+j).html("<span class='td-md borde' id='td"+(i-1)+"-"+j+"'>"+numero[0]+" &#8595</span>"); 
                    $('#'+cordenadaArriba).append("<span class='noPregunta sp'>"+numero[0]+" &#8595 </span>");
                    $('#Verticales').append(numero[0]+".-"+preguntas[numero[0]-1] +"<br>");
                    verticales.push(numero[0]);
                    cordenadasVerticales.push(cordenadaActual);
                }else if (arriba==0 && numero[1]== abajo[0] && abajo!=0 ) {
                    //$('#td'+(i-1)+"-"+j).html("<td class='td-md borde' id='td"+(i-1)+"-"+j+"'>"+numero[1]+" &#8595</td>");
                    //$('#td'+(i-1)+"-"+j).append(numero[1] +'&#8595;');
                    $('#'+cordenadaArriba).append("<span class='noPregunta sp' >"+numero[1]+"&#8595</span>");
                    $('#Verticales').append(numero[1]+".-"+preguntas[numero[1]-1] +"<br>");
                    verticales.push(numero[1]);
                    cordenadasVerticales.push(cordenadaActual);
                }else if (arriba==0 && numero[0] == abajo[1] && abajo!=0 ) {
                    //$('#td'+(i-1)+"-"+j).append(numero[0] +"&#8595;");
                    //$('#td'+(i-1)+"-"+j).html("<span class='td-md borde' id='td"+(i-1)+"-"+j+"'>"+numero[0]+" &#8595</span>");
                    $('#'+cordenadaArriba).append("<span class='noPregunta sp'>"+numero[0]+" &#8595</span>");
                    $('#Verticales').append(numero[0]+".-"+preguntas[numero[0]-1] +"<br>");
                    verticales.push(numero[0]);
                    cordenadasVerticales.push(cordenadaActual);
                }
                //Añadir Horizontales
                var cordenadaIzquierda=i+"-"+(j-1);
                if(izquierda==0 && numero[0]==derecha[0] && derecha!=0 ){
                    //$('#td'+(i)+""+(j-1)).html("<td class='td-md-plus borde' id='td"+i+""+(j-1)+"'>"+grid[i][j]+" &#8594</td>");
                    //$('#td'+i+"-"+(j-1)).html("<td class='td-md borde'>"+numero[0]+"&#8594 </td>");
                    //('#td'+i+"-"+(j-1)).append(numero[0] +"&#8594;");
                    $('#'+cordenadaIzquierda).append("<span class='noPregunta sp'>"+ numero[0] +" &#8594</span>");
                    $('#Horizontales').append(numero[0]+".-"+preguntas[numero[0]-1] +"<br>");
                    horizontales.push(numero[0]);
                    cordenadasHorizontales.push(cordenadaActual);
                }else if (izquierda==0 && numero[1]==derecha[0] && derecha!=0) {
                    //$('#td'+i+"-"+(j-1)).html("<td class='td-md'>"+numero[1]+"&#8594 </td>");
                    //('#td'+i+"-"+(j-1)).append(numero[1]+" &#8594;");
                    $('#'+cordenadaIzquierda).append("<span class='noPregunta sp'>"+ numero[1] +" &#8594</span>");
                    $('#Horizontales').append(numero[1]+".-"+preguntas[numero[1]-1] +"<br>");
                    horizontales.push(numero[1]);
                    cordenadasHorizontales.push(cordenadaActual);
                }else if (izquierda==0 && numero[0]==derecha[1] && derecha!=0) {
                    //$('#td'+i+"-"+(j-1)).html("<td class='td-md'>"+numero[1]+"&#8594 </td>");
                    //('#td'+i+"-"+(j-1)).append(numero[1]+" &#8594;");
                    $('#'+cordenadaIzquierda).append("<span class='noPregunta sp'>"+ numero[0] +" &#8594</span>");
                    $('#Horizontales').append(numero[0]+".-"+preguntas[numero[0]-1] +"<br>");
                    horizontales.push(numero[0]);
                    cordenadasHorizontales.push(cordenadaActual);
                }else if (izquierda==0 && numero[1]==derecha[1] && derecha!=0 && numero[1]!=null) {
                    //$('#td'+i+"-"+(j-1)).html("<td class='td-md'>"+numero[1]+"&#8594 </td>");
                    //('#td'+i+"-"+(j-1)).append(numero[1]+" &#8594;");
                    $('#'+cordenadaIzquierda).append("<span class='noPregunta sp'>"+ numero[1] +" &#8594</span>");
                    $('#Horizontales').append(numero[1]+".-"+preguntas[numero[0]-1] +"<br>");
                    horizontales.push(numero[1]);
                    cordenadasHorizontales.push(cordenadaActual);
                }
                            
            }
        }
        $('#tablaCrucigrama').append("</td>");
    }
    $('#tablaCrucigrama').append("<tr> </tr>");
}



//variables globales
var cordenada;
var noPregunta;
var direccionRespuesta;

/*$('.noPregunta').click(function(){
    $('.noPregunta').css('background','#e3e7ed')
    $(this).css('background', '#A4A4A4');
    cordenada=this.parentElement.id.split("-");
    noPregunta=$(this)[0].innerText.substring(0, 1);
    $("#Verificar").removeAttr('disabled');
    direccionRespuesta= getPosition(noPregunta);
    if (direccionRespuesta=="vertical") {
        var nuevaCordedana=(parseInt(cordenada[0])+1)+"-"+cordenada[1];
        $('#'+nuevaCordedana).children().focus();
        foco= nuevaCordedana.split("-");
    }else if (direccionRespuesta=="horizontal") {
        var nuevaCordedana=cordenada[0]+"-"+(parseInt(cordenada[1])+1);
        $('#'+nuevaCordedana).children().focus();
        foco= nuevaCordedana.split("-");
    }
});*/


$('#Verificar').click(function () {
    for (var i = 0; i < horizontales.length; i++) {
        var resp= respuestas[horizontales[i]-1];
        var id= cordenadasHorizontales[i].split("-");
        for (var j = 0; j < resp.length; j++) {
            var letra= $('#'+id[0]+'-'+(parseInt(id[1])+j))[0].innerText;   
            if (letra==resp[j]) {
                $('#'+id[0]+'-'+(parseInt(id[1])+j)).children().css('background',"#01DF01");
            }else if(letra!= resp[j] && letra!="") {
                $('#'+id[0]+'-'+(parseInt(id[1])+j)).children().css('background',"#FF0000");
            }else if(letra==""){
                $('#'+id[0]+'-'+(parseInt(id[1])+j)).children().css('background',"#FFF");
            }
        }
    }
    for (var i = 0; i < verticales.length; i++) {
        var resp= respuestas[verticales[i]-1];
        var id= cordenadasVerticales[i].split("-");
        for (var j = 0; j < resp.length; j++) {
            var letra= $('#'+(parseInt(id[0])+j)+"-"+id[1])[0].innerText;   
            if (letra==resp[j]) {
                $('#'+(parseInt(id[0])+j)+"-"+id[1]).children().css('background',"#01DF01");
            }else if(letra!= resp[j] && letra!="") {
                $('#'+(parseInt(id[0])+j)+"-"+id[1]).children().css('background',"#FF0000");
            }else if(letra==""){
                $('#'+(parseInt(id[0])+j)+"-"+id[1]).children().css('background',"#FFF");
            }
        }
    }
    noChecks++;
    $('#noChecks').html(' Numero de Checks='+ noChecks);
});
$('.casilla').focus(function () {
    foco= this.parentElement.id.split("-");
});
$('.casilla').click(function () {
    var cordenada=this.parentElement.id.split("-");
    var numeroRespuesta= grid[cordenada[0]][cordenada[1]];
    foco= cordenada;
    direccionRespuesta=getPosition(numeroRespuesta);
    if (direccionRespuesta=="vertical") {
        var nuevaCordedana=(parseInt(cordenada[0])+1)+"-"+cordenada[1];
        //$('#'+nuevaCordedana).children().focus();
        foco= nuevaCordedana.split("-");
    }else if (direccionRespuesta=="horizontal") {
        var nuevaCordedana=cordenada[0]+"-"+(parseInt(cordenada[1])+1);
        //$('#'+nuevaCordedana).children().focus();
        foco= nuevaCordedana.split("-");
    }
});

//@param1 (int pregunta)
function getPosition(pregunta) {
    for (var i = 0; i < verticales.length; i++) {
        if (pregunta== verticales[i]) {
            return "vertical";
        }
    }
    for (var i = 0; i < horizontales.length; i++) {
        if (pregunta==horizontales[i]) {
            return "horizontal";
        }
    }
}


var tecla;
$(".casilla").keyup(function(){
    var id=this.parentElement.id.split("-");
    if (tecla!=40 && tecla!=39 && tecla!=38 && tecla!=37 && tecla!=8 && tecla!=17 && tecla!=18 && tecla!=16 && tecla!=9 && tecla!=33 && tecla!=34 && tecla!=13 && tecla!=46 ) {
        if(tecla==32){
            var identificador=this.parentElement.id;
            $('#'+identificador ).children().text('_');
        }
        if (direccionRespuesta=="vertical" ) {
            var nuevoId= parseInt(id[0])+1 +"-"+id[1];
            $('#'+nuevoId).children().focus();
        }else if (direccionRespuesta=="horizontal") {
            var nuevoId=id[0]+"-"+(parseInt(id[1])+1);
            $('#'+nuevoId).children().focus();
        }
        this.id="casillaOcupada";
    }else if(tecla==13 || tecla==46 || tecla==8){
        var id=this.parentElement.id;
        $('#'+id).children().text("");

    }
});
$('.casilla').keydown(function(e){
    var id=this.parentElement.id.split("-");
    tecla= e.which;
    if(tecla==40){
        //tecla abajo
        var nuevoId= parseInt(id[0])+1 +"-"+id[1];
        $('#'+nuevoId).children().focus();
        direccionRespuesta="vertical";
    }else if(tecla==38){
        //tecla arriba
        var nuevoId= parseInt(id[0])-1 +"-"+id[1];
        $('#'+nuevoId).children().focus();
        direccionRespuesta="vertical";
    }else if(tecla==37){
        //tecla izquierda
        var nuevoId= id[0] +"-"+ (parseInt(id[1])-1);
        $('#'+nuevoId).children().focus();
        direccionRespuesta="horizontal";
    }else if(tecla==39){
        //tecla derecha
        var nuevoId= id[0]+"-"+(parseInt(id[1])+1);
        $('#'+nuevoId).children().focus();
        direccionRespuesta="horizontal";
    }else if(tecla==8 || tecla==46 || tecla==13){
        this.id="casillaVacia"
    }
});
$('.casilla').keypress(function(){
    if (tecla!=13) {
        $(this).text("");
    }   
});